const voucherModel= require('../models/voucherModel');

const mongoose= require('mongoose');

const createVoucher = async (req, res) => {
    // B1: Thu thap du lieu
    const {
       mavoucher ,
        phantramgiamgia,
        ghichu
    } = req.body;

    // B2: Validate du lieu
    try {
        // B3: Xu ly du lieu

        var newVoucher = {
            maVoucher: mavoucher,
            phanTramGiamGia: phantramgiamgia,
            ghiChu: ghichu
        }

        const result = await voucherModel.create(newVoucher);

        return res.status(201).json({
            message: "Tao voucher thanh cong",
            data: result
        })
    } catch (error) {
        // Dung cac he thong thu thap loi de thu thap error

        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getAllVouchers = async (req, res) => {
    // B1: Thu thap du lieu
    // B2: Validate du lieu
    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.find();

        return res.status(200).json({
            message: "Lay danh sach voucher thanh cong",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}

const getVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherid = req.params.voucherid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            message: "Voucher ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.findById(voucherid);

        if(result) {
            return res.status(200).json({
                message: "Lay thong tin voucher thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher"
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


const updateVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherid = req.params.voucherid;
    const {
        mavoucher,
        phantramgiamgia,
        ghichu
    } = req.body;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            message: "Voucher ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        var newUpdateVoucher = {};
        if(mavoucher) {
            newUpdateVoucher.maVoucher = mavoucher
        }
        if(phantramgiamgia) {
            newUpdateVoucher.phanTramGiamGia = phantramgiamgia
        }
        if(ghichu) {
            newUpdateVoucher.ghiChu = ghichu
        }

        const result = await voucherModel.findByIdAndUpdate(voucherid, newUpdateVoucher);

        if(result) {
            return res.status(200).json({
                message: "Update thong tin voucher thanh cong",
                data: result
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}   

const deleteVoucherById = async (req, res) => {
    // B1: Thu thap du lieu
    const voucherid = req.params.voucherid;

    // B2: Validate du lieu
    if(!mongoose.Types.ObjectId.isValid(voucherid)) {
        return res.status(400).json({
            message: "Voucher ID khong hop le"
        })
    }

    // B3: Xu ly du lieu
    try {
        const result = await voucherModel.findByIdAndRemove(voucherid);

        if(result) {
            return res.status(200).json({
                message: "Xoa thong tin voucher thanh cong"
            })
        } else {
            return res.status(404).json({
                message: "Khong tim thay thong tin voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            message: "Co loi xay ra"
        })
    }
}


module.exports={ createVoucher, getAllVouchers, getVoucherById, updateVoucherById,deleteVoucherById};

