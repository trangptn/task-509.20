const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    maVoucher: {
        type: String,
        required: true,
        unique: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    },
});

module.exports = mongoose.model("Voucher", voucherSchema);
