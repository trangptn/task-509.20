const express= require("express");
const { getAllVouchers, createVoucher,getVoucherById,updateVoucherById,deleteVoucherById} = require("../controllers/voucherController");

const voucherRouter= express.Router();
voucherRouter.get("/",getAllVouchers);
voucherRouter.post("/",createVoucher);
voucherRouter.get("/:voucherid",getVoucherById);
voucherRouter.put("/:voucherid",updateVoucherById);
voucherRouter.delete("/:voucherid",deleteVoucherById);


module.exports= voucherRouter;