const express= require("express");

const drinkRouter= express.Router();

const {createDrink, getAllDrinks,getDrinkById, updateDrinkById,deleteDrinkById}= require("../controllers/drinkController");

drinkRouter.post("", createDrink);
drinkRouter.get("",getAllDrinks);
drinkRouter.get("/:drinkid",getDrinkById);
drinkRouter.put("/:drinkid",updateDrinkById);
drinkRouter.delete("/:drinkid",deleteDrinkById);
module.exports= drinkRouter;