//B1: Import thư viện express
//import express from express
const express = require('express');

const mongoose = require('mongoose');
//B2: Khởi tạo app express
const app = new express();

//B3: Khai báo cổng để chạy api
const port = 8000;

const drinkRouter=require('./app/routers/drinkRouter');

const voucherRouter=require('./app/routers/voucherRouter');

const drinkModel=require('./app/models/drinkModel');
const voucherModel=require('./app/models/voucherModel');

app.use(express.json());

mongoose.connect("mongodb://127.0.0.1:27017/R46_Course_Review")
    .then(() => {
        console.log("Connect mongoDB Successfully");
    })
    .catch((err) => {
        console.log(err);
    });



app.use("/drinks", drinkRouter);
app.use("/vouchers",voucherRouter);

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})

